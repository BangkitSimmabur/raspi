# Import Libraries

from pyrebase import pyrebase
import RPi.GPIO as GPIO
import time
#from firebase import firebase


GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
# Firebase Configuration
config = {
 "apiKey": "AIzaSyDrthAetXFznwthYGfGsEhuKjuFhIqQnF8",
 "authDomain": "sikolamp-fd2e2.firebaseapp.com",
  "databaseURL": "https://sikolamp-fd2e2.firebaseio.com",
  "projectId": "sikolamp-fd2e2",
  "storageBucket": "sikolamp-fd2e2.appspot.com",
 "messagingSenderId": "1021687733158",
  "appId": "1:1021687733158:web:dffd472bf1c2ed16"
}
#pin LDR
a_pin = 2
b_pin = 3
#pin LED
led_pin = 23 #lampu1
led_pin1 = 22 #lampu2
#pin button
button = 18 #button 1
#setup led
GPIO.setup(led_pin, GPIO.OUT)
GPIO.setup(led_pin1, GPIO.OUT)
#firebase
firebase = pyrebase.initialize_app(config)
#setup button
GPIO.setup(button, GPIO.IN, pull_up_down=GPIO.PUD_UP)

db = firebase.database()
#deklarasi pembacaan ldr untuk fitur otomatis
def discharge():
        GPIO.setup(a_pin, GPIO.IN)
        GPIO.setup(b_pin, GPIO.OUT)
        GPIO.output(b_pin, False)
        time.sleep(0.005)

def charge_time():
        GPIO.setup(b_pin, GPIO.IN)
        GPIO.setup(a_pin, GPIO.OUT)
        count = 0
        GPIO.output(a_pin, True)
        while not GPIO.input(b_pin):
                count = count + 1
        return count
def analog_read():
        discharge()
        return charge_time()

while(True):
    print(analog_read())
    #pengambilan value untuk lampu
    lamp1 = db.child("t_lampu").child("bangkit").child("kamar").child("lampu").get()
    #pengambilan value untuk fitur
    auto1 = db.child("t_lampu").child("bangkit").child("kamar").child("fitur").get()
    # loop untuk melakukan cek status lampu pada basis data dan menyalakan/ memedamkan lampu sesuai data pada basis data
    for status_lampu in lamp1.each():
        if(status_lampu.val() == "ON"):
            print("Lampu kamar  Menyala")
            GPIO.output(led_pin, GPIO.HIGH)
            input_state = GPIO.input(button)
            # melakukan update ke basis data menggunakan switch/button/saklar pada saat kondisi lampu menyala menjadi padam
            if(input_state == False):
                db.child("t_lampu").child("bangkit").child("kamar").child("lampu").update({"s_lampu":"OFF"})
        else:
            print("Lampu kamar Padam")
            GPIO.output(led_pin, GPIO.LOW)
            # melakukan update ke basis data menggunakan switch/button/saklar pada saat kondisi lampu padam menjadi menyala
            input_state = GPIO.input(button)
            if(input_state == False):
                db.child("t_lampu").child("bangkit").child("kamar").child("lampu").update({"s_lampu":"ON"})

    for status in auto1.each():
        if(status.val() == "ON"):
            print("Fitur Otomatis Menyala")
            a = analog_read()
            b = analog_read()
            if a > 5 and b > 5: # kondisi untuk fitur otomatis
                db.child("t_lampu").child("bangkit").child("kamar").child("lampu").update({"s_lampu":"ON"})
            else:
                db.child("t_lampu").child("bangkit").child("kamar").child("lampu").update({"s_lampu":"OFF"})
        else:
            print("Fitur Otomatis Mati") 

    lamp2 = db.child("t_lampu").child("bangkit").child("teras").child("lampu").get()
    #pengambilan value untuk fitur
    auto2 = db.child("t_lampu").child("bangkit").child("teras").child("fitur").get()
    # loop untuk melakukan cek status lampu pada basis data dan menyalakan/ memedamkan lampu sesuai data pada basis data
    for status_lampu in lamp2.each():
        if(status_lampu.val() == "ON"):
            print("Lampu teras  Menyala")
            GPIO.output(led_pin1, GPIO.HIGH)
            input_state = GPIO.input(button)
        else:
            print("Lampu teras padam")
            GPIO.output(led_pin1, GPIO.LOW)

    for status in auto2.each():
        if(status.val() == "ON"):
            print("Fitur Otomatis Menyala")
            a = analog_read()
            b = analog_read()
            if a > 5 and b > 5: # kondisi untuk fitur otomatis
                db.child("t_lampu").child("bangkit").child("teras").child("lampu").update({"s_lampu":"ON"})
            else:
                db.child("t_lampu").child("bangkit").child("teras").child("lampu").update({"s_lampu":"OFF"})
        else:
            print("Fitur Otomatis Mati") 

